Tiny module that let administrators define with more granularity the number of watchdog entries to keep in database.

Installation and configuration

1) Enable the module from modules page
2) Go to 'admin/config/development/logging/dsc' to configure